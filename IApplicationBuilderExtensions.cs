﻿using FrontendProxy.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy
{
    public static class IApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseProxy(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetRequiredService<IOptions<ProxySettings>>();
            return app.ConfigureProxy(options);
        }

        public static IApplicationBuilder ConfigureProxy(this IApplicationBuilder app, IOptions<ProxySettings> options)
        {
            var settings = options.Value;

            app.UseEndpoints(endpoints =>
            {
                if (settings.UseProxy)
                {
                    var controller = settings.UseAuthorization ? "ProxyAuthorized" : "Proxy";

                    endpoints.MapControllerRoute("Proxy", "", defaults: new { controller, action = "Index", });
                    endpoints.MapControllerRoute("Proxy (static files)", "static/{*path}", defaults: new { controller = "Proxy", action = "Index", });

                    foreach(var additionalFolder in settings.ProxyFolders)
                    {
                        endpoints.MapControllerRoute($"Proxy ({additionalFolder})", additionalFolder + "/{*path}", defaults: new { controller = "Proxy", action = "Index", });
                    }

                }
                else
                {
                    var controller = settings.UseAuthorization ? "StaticFilesAuthorized" : "StaticFiles";

                    endpoints.MapControllerRoute("Static files", "", defaults: new { controller, action = "Index", });

                    var root = settings.UseParentFolder ? Directory.GetParent(Directory.GetCurrentDirectory()).FullName : Directory.GetCurrentDirectory();
                    var path = Path.Combine(root, settings.StaticFilesFolder);

                    try
                    {
                        app.UseStaticFiles(new StaticFileOptions
                        {
                            FileProvider = new PhysicalFileProvider(path),
                        });
                    }
                    catch (Exception e)
                    {
                        throw new FrontendProxyException($"Could not initialize PhysicalFileProvider with root: {path}", e);
                    }
                }
            });

            return app;
        }

    }
}
