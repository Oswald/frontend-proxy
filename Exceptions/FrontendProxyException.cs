﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy.Exceptions
{
    public class FrontendProxyException : Exception
    {
        public FrontendProxyException(string message) : base(message)
        {

        }

        public FrontendProxyException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
