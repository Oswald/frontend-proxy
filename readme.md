# Frontend proxy

This library allows you to easily use React frontend with .NET Core backend. Use proxy for development and static files for production. The library is written for .NET Core 3.1, but also supports .NET 5.

### Install Nuget-package
```
Install-Package FrontendProxy
```

### Configure 

appsettings.json (only up-to-date for versions >= 1.0.7)
```
"Proxy": {
    "UseProxy": true,                        //If development proxy is used. Default: true
    "UseAuthorization": false                //If true, uses the 'Authorize' -attribute for controllers. Default: false
    "ProxyUrl": "http://localhost:3000",     //Frontend development server location. Defaults: 'http://localhost:3000'
    "ProxyFolders": [ "images" ],            //To map additional proxy folders (in addition to "static")
    "StaticFilesFolder": "wwwroot"           //Frontend files folder name. Defaults: 'wwwroot'
    "UseParentFolder": false                 //If false, searches static files inside project. Set to 'true', if backend and frontend are in paraller folders. Default: false
}
```

#### Add to startup.cs

To **ConfigureServices()**
```
services.ConfigureProxy(Configuration);
```
For 1.0.7 and older versions
```
services.Configure<ProxySettings>(Configuration.GetSection("Proxy"));
```

To **Configure()**
```
app.UseProxy()
```

For 1.0.6 and older versions:
```
public void Configure(... , IOptions<ProxySettings> settings) 
{
    ...
    app.ConfigureProxy(settings);
}
```

