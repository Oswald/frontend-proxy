﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy.Controllers
{
    [Authorize]
    public class StaticFilesAuthorizedController : StaticFilesController
    {
        public StaticFilesAuthorizedController(IOptions<ProxySettings> options) : base(options)
        {

        }
    }
}
