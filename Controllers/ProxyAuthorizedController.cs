﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy
{
    [Authorize]
    public class ProxyAuthorizedController : ProxyController
    {
        public ProxyAuthorizedController(IOptions<ProxySettings> settings) : base(settings) 
        {
        }
    }
}
