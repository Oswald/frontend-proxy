﻿using FrontendProxy.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendProxy
{
    public class ProxyController : Controller
    {
        private readonly ProxySettings settings;
        public ProxyController(IOptions<ProxySettings> options)
        {
            settings = options.Value;
        }

        public async Task<ActionResult> Index()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var res = await client.GetAsync(settings.ProxyUrl + Request.Path.ToString());
                    var contentType = res.Content.Headers.ContentType;
                    var content = await res.Content.ReadAsByteArrayAsync();
                    return File(content, contentType.MediaType);
                }
            } catch (Exception e)
            {
                throw new FrontendProxyException($"Could not get files from frontend file server. Is the server running on {settings.ProxyUrl}?", e);
            }
        }
    }
}
