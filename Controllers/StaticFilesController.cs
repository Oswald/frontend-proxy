﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy
{
    public class StaticFilesController : Controller
    {
        private readonly string indexFilePath;
        public StaticFilesController(IOptions<ProxySettings> options)
        {
            var path = options.Value.UseParentFolder? Directory.GetParent(Directory.GetCurrentDirectory()).FullName : Directory.GetCurrentDirectory();
            indexFilePath = Path.Combine(path, options.Value.StaticFilesFolder, "index.html");
        }

        public ActionResult Index()
        {
            Response.Headers.Add("Content-Disposition", new System.Net.Mime.ContentDisposition { Inline = true, FileName = "index.html" }.ToString());
            return File(System.IO.File.ReadAllBytes(indexFilePath), "text/html");
        }
    }
}
