﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendProxy
{
    public static class IServiceCollectionExtensions
    {
        public static void ConfigureProxy(this IServiceCollection collection, IConfiguration configuration)
        {
            collection.Configure<ProxySettings>(configuration.GetSection("Proxy"));
        }
    }
}
