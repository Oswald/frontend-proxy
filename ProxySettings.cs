﻿namespace FrontendProxy
{
    public class ProxySettings
    {
        public bool UseProxy { get; set; } = true;
        public bool UseAuthorization { get; set; } = false;
        public string ProxyUrl { get; set; } = "http://localhost:3000";
        public string[] ProxyFolders { get; set; } = new string[0];
        public string StaticFilesFolder { get; set; } = "wwwroot";
        public bool UseParentFolder { get; set; } = false;
    }
}